#pragma once
#include <vector>
#include <string> 
#include <memory>
#include <geomCurve.h>


class  Controller{
    public:
    void getPoint(std::shared_ptr<GeomCurve> geomCurve,float t){
        if(geomCurve->checkRadius()){
             geomCurve->getPoint(t);
        }
        else{
            geomCurve->~GeomCurve();
            std::cout<<"incorrect value of radius"<<std::endl;
        }
        
        
    };

        void getDerivative(std::shared_ptr<GeomCurve> geomCurve,float t){
        if(geomCurve->checkRadius()){
             geomCurve->getDerivative(t);
        }
        else{
            geomCurve->~GeomCurve();
            std::cout<<"incorrect value of radius"<<std::endl;
        }
        
        
    };

            std::string getNameCurve(std::shared_ptr<GeomCurve> geomCurve){
        if(geomCurve->checkRadius()){
             return geomCurve->getNameCurve();
             
        }
        else{
            geomCurve->~GeomCurve();
            std::cout<<"incorrect value of radius"<<std::endl;
        }
        
        
    };
    


    

   
};