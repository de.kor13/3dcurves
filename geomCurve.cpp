#pragma once
#include <vector>
#include <string> 
#include <memory>
#include <math.h> 

#include <point.h>
#include <geomCurve.h>

std::shared_ptr<Point> Circle::getPoint(float t){
    std::shared_ptr<Point> point=std::make_shared<Point>(cos(2 * t) * radius,sin(2 * t) * radius,0);
    point->printPoint();
    return point;
}
bool Circle::checkRadius(){
    if(radius>0)
    return true;
    else
    return false;

}

std::vector<float> Circle::getDerivative(float t){
   
    std::vector<float> vec{-2*radius*sin(2*t),2*radius*cos(2*t),0};
    std::cout<<"{"<<-2*radius*sin(2*t)<<","<<2*radius*cos(2*t)<<","<<0<<"}"<<std::endl;
    return vec;
}

std::string Circle::getNameCurve(){
    return nameCurve;
}




std::shared_ptr<Point> Ellipse::getPoint(float t){
        std::shared_ptr<Point> point=std::make_shared<Point>(smallAxis * cos(t), bigAxis * sin(t), 0); 
        point->printPoint();
        return point;
    }

bool Ellipse::checkRadius(){
    if(smallAxis>0 && bigAxis>0)
    return true;
    else
    return false;

}
std::vector<float> Ellipse::getDerivative(float t){
   
    std::vector<float> vec{-smallAxis*sin(t), bigAxis*cos(t),0};
    std::cout<<"{"<<-smallAxis*sin(t)<<","<<bigAxis*cos(t)<<","<<0<<"}"<<std::endl;
    return vec;
}

std::string Ellipse::getNameCurve(){
    return nameCurve;
}


std::shared_ptr<Point> Helix::getPoint(float t)  {
    std::shared_ptr<Point> point=std::make_shared<Point>(radius*cos(t),radius*sin(t) ,step * t);
    point->printPoint();
    return point;
     }

bool Helix::checkRadius(){
    if(radius>0)
    return true;
    else
    return false;

}
std::vector<float> Helix::getDerivative(float t){
   
    std::vector<float> vec{-radius*sin(t),radius*cos(t),step};
    std::cout<<"{"<<-radius*sin(t)<<","<<radius*cos(t)<<","<<step<<"}"<<std::endl;
    return vec;
}
std::string Helix::getNameCurve(){
    return nameCurve;
}