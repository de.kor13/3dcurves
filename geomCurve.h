#pragma once
#include <vector>
#include <string> 
#include <memory>
#include <math.h> 

#include <point.h>
#define PI 3.14159265


class  GeomCurve{
    public:
    virtual ~GeomCurve(){};
    virtual std::shared_ptr<Point> getPoint(float t)=0;
    virtual std::vector<float> getDerivative(float t)=0;
    virtual bool checkRadius()=0; 
    virtual std::string getNameCurve()=0;
   
    float radiusCircle;
    
     
};

class Circle: public GeomCurve{
    public:
    Circle(float r): radius(r){  
        GeomCurve::radiusCircle=r;
    };
    bool checkRadius() override;

   std::shared_ptr<Point> getPoint(float t) override;
   std::vector<float> getDerivative(float t) override;
   std::string getNameCurve() override;
   


  

    private:
    float radius;
    std::string nameCurve="Circle";
  

};



class Ellipse: public GeomCurve{
    public:
    Ellipse(float a,float b):smallAxis(a),bigAxis(b){

    };
    bool checkRadius() override;
    
    std::shared_ptr<Point> getPoint(float t) override;
    std::vector<float> getDerivative(float t) override;
    std::string getNameCurve() override;
    
    private:
    float smallAxis;
    float bigAxis;
    std::string nameCurve="Ellipse";
 

    

};

class Helix: public GeomCurve{
    public:
    Helix(float r,float z):radius(r),step(z){

    };
    bool checkRadius() override;
     std::shared_ptr<Point> getPoint(float t) override;
    std::vector<float> getDerivative(float t) override;
    std::string getNameCurve() override;



    private:
    float radius;
    float step;
    std::string nameCurve="Helix";
   

};

