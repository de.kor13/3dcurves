#include <iostream>
#include <vector>
#include <string> 
#include <memory>
#include <controller.h>
#include <geomCurve.h>
#include <iterator>
#include <algorithm>
#define PI 3.14159265


struct sortByCircle {
    bool operator()(std::shared_ptr<GeomCurve> a, std::shared_ptr<GeomCurve> b) {
        return a->radiusCircle < b->radiusCircle;
    }
};

void remove(std::vector<std::shared_ptr<GeomCurve>>& vec, size_t pos)
{
    std::vector<std::shared_ptr<GeomCurve>>::iterator it = vec.begin();
    std::advance(it, pos);
    vec.erase(it);
}

int main()
{
std::shared_ptr<Controller> controller=std::make_shared<Controller>();
std::vector<std::shared_ptr<GeomCurve>> vec1;
std::vector<std::shared_ptr<GeomCurve>> vec2;


std::shared_ptr<Circle> circle=std::make_shared<Circle>(10);
std::shared_ptr<Ellipse> ellipse=std::make_shared<Ellipse>(10,12);
std::shared_ptr<Helix> helix=std::make_shared<Helix>(10,1);
std::shared_ptr<Circle> circle1=std::make_shared<Circle>(5);
std::shared_ptr<Circle> circle2=std::make_shared<Circle>(100);
std::shared_ptr<Ellipse> ellipse1=std::make_shared<Ellipse>(12,16);
std::shared_ptr<Helix> helix1=std::make_shared<Helix>(1,20);
std::shared_ptr<Ellipse> ellipse2=std::make_shared<Ellipse>(10,112);
std::shared_ptr<Circle> circle3=std::make_shared<Circle>(1);
std::shared_ptr<Circle> circle4=std::make_shared<Circle>(19);



vec1.push_back(ellipse);
vec1.push_back(circle);
vec1.push_back(helix);
vec1.push_back(circle1);
vec1.push_back(circle2);
vec1.push_back(ellipse1);
vec1.push_back(helix1);
vec1.push_back(ellipse2);
vec1.push_back(circle3);
vec1.push_back(circle4);




for(auto i: vec1){
    std::cout<<"point: ";
    controller->getPoint(i,PI/4);
} 
for(auto i: vec1){
    std::cout<<"derivative: ";
    controller->getDerivative(i,PI/4);
} 



std::vector<int> idel;
int j=0;

for (auto i = vec1.begin(); i != vec1.end(); ++i)
{
      if(controller->getNameCurve(*i)=="Circle"){    
       vec2.push_back(std::move(*i));     
        idel.push_back(j);
     // vec1.erase(i);/// сократить старый вектор.      
      }
 j++;
}

for(int i=0;i<idel.size();i++){
   vec1.erase(vec1.begin()+idel[i]-i);
}

for(int i=0;i<vec1.size();i++){
    std::cout<<"ObjOfVec1 = "<<controller->getNameCurve(vec1[i])<<std::endl;
}




for(auto i: vec2){
 //   std::cout<<controller->getNameCurve(i)<<std::endl;
    std::cout<<"radiusCircle = "<<i->radiusCircle<<std::endl;
}

std::sort(vec2.begin(), vec2.end(), sortByCircle());

for(auto i: vec2){
    std::cout<<"radiusCircleSORT = "<<i->radiusCircle<<std::endl;
}

float sumRadius{};

for(auto i: vec2){
    
    sumRadius+=i->radiusCircle;
}
std::cout<<"Sum radius vec2 = "<< sumRadius<<std::endl;




return 0;
 
}

